from django.test import TestCase
from .models import Todo
from faker import Faker
from querytools.tools import (
    group_by_and_aggregate,
    group_by_and_annotate,
    as_timeseries,
    pivot_table,
    periodic_breakdown
)
import datetime

fake = Faker()

def create_world():
    '''Creates a known world of TODOS'''

    todos = [
        # jack:
        {"due": "2018-11-01", "time": "15:05", "size": 1, "status": "N", "owner": "Jack" },
        {"due": "2018-11-01", "time": "15:05", "size": 2, "status": "P", "owner": "Jack" },
        {"due": "2018-11-02", "time": "15:05", "size": 3, "status": "P", "owner": "Jack" },
        {"due": "2018-11-04", "time": "15:05", "size": 1, "status": "D", "owner": "Jack" },
        {"due": "2018-11-04", "time": "15:05", "size": 1, "status": "D", "owner": "Jack" },
        {"due": "2018-11-05", "time": "15:05", "size": 2, "status": "D", "owner": "Jack" },
        # Jane:
        {"due": "2018-11-01", "time": "15:05", "size": 3, "status": "P", "owner": "Jane" },
        {"due": "2018-11-02", "time": "15:05", "size": 1, "status": "P", "owner": "Jane" },
        {"due": "2018-11-01", "time": "15:05", "size": 2, "status": "D", "owner": "Jane" },
        {"due": "2018-11-03", "time": "15:05", "size": 3, "status": "D", "owner": "Jane" },
        {"due": "2018-11-05", "time": "15:05", "size": 2, "status": "P", "owner": "Jane" },
        {"due": "2018-11-08", "time": "15:05", "size": 2, "status": "P", "owner": "Jane" },
        {"due": "2018-11-09", "time": "15:05", "size": 2, "status": "P", "owner": "Jane" },
        {"due": "2018-11-10", "time": "15:05", "size": 2, "status": "P", "owner": "Jane" },

        {"due": "2019-11-05", "time": "15:05", "size": 2, "status": "P", "owner": "Jane" },
        {"due": "2019-11-08", "time": "15:05", "size": 2, "status": "P", "owner": "Jane" },
        {"due": "2019-11-09", "time": "15:05", "size": 2, "status": "P", "owner": "Jane" },
    ]
    for todo in todos:
        data = todo
        data.update({
            "title": fake.text()[:29],
            "due_time": "{} {}".format(
                todo.get("due"),
                todo.get("time")
            )
        })
        del data["time"]
        Todo.objects.create(**data)


class QueryToolsTestCase(TestCase):

    def setUp(self):
        create_world()

    def test_world_exists(self):
        self.assertEqual(Todo.objects.count(), 17)

    def test_group_by_and_aggregate(self):
        qs = Todo.objects.all()
        total_size = group_by_and_aggregate(
            qs,
            'size',
            'Sum'
        )
        self.assertEqual(total_size, 33)

    def test_group_by_and_annotate(self):
        qs = Todo.objects.all()
        result = group_by_and_annotate(
            qs,
            'status',
            'Count'
        )
        expected_results = {
            'D': 5,
            'N': 1,
            'P': 11
        }
        for (key, value) in expected_results.items():
            self.assertEqual(
                result.get(key),
                value
            )


class TimeSeriesTestCase(TestCase):

    def setUp(self):
        create_world()
        qs = Todo.objects.filter(owner="Jack")
        self.result = as_timeseries(
            qs,
            'due_time',
            'id',
            'Count',
            '2018-11-01',
            '2018-11-10'
        )

    def test_all_days_in_range_are_populates(self):
        self.assertEqual(
            self.result[0].get("x"),
            "2018-11-01"
        )
        self.assertEqual(
            self.result[-1:][0].get("x"),
            "2018-11-10"
        )

    def test_series_is_correct(self):
        series = [i.get("y") for i in self.result]
        expected_series = [2.0, 1.0, 0, 2.0, 1.0, 0, 0, 0, 0, 0]
        self.assertEqual(
            series,
            expected_series
        )

    def test_it_works_on_a_date_field_too(self):
        qs = Todo.objects.filter(owner="Jack")
        result = as_timeseries(
            qs,
            'due',
            'id',
            'Count',
            '2018-11-01',
            '2018-11-10'
        )
        series = [i.get("y") for i in result]
        expected_series = [2.0, 1.0, 0, 2.0, 1.0, 0, 0, 0, 0, 0]
        self.assertEqual(
            series,
            expected_series
        )

    def test_it_works_with_sum(self):
        qs = Todo.objects.filter(owner="Jack")
        result = as_timeseries(
            qs,
            'due',
            'size',
            'Sum',
            '2018-11-01',
            '2018-11-10'
        )
        series = [i.get("y") for i in result]
        expected_series = [3.0, 3.0, 0, 2.0, 2.0, 0, 0, 0, 0, 0]
        self.assertEqual(
            series,
            expected_series
        )


def serialize_todo(todo):
    return {
        "title": todo.title
    }

class PivotfieldsTestCase(TestCase):

    def setUp(self):
        create_world()

    def test_single_level_level_pivot(self):
        qs = Todo.objects.all()
        result = pivot_table(qs, 'owner')
        self.assertEqual(
            len(result.get('Jack')),
            6
        )
        self.assertEqual(
            len(result.get('Jane')),
            11
        )

    def test_multilevel_level_pivot(self):
        qs = Todo.objects.all()
        result = pivot_table(qs, 'owner,status')

        expected_results = {
            "Jack": [
                ("N", 1),
                ("P", 2),
                ("D", 3)
            ]
        }
        for status, expected_count in expected_results.get("Jack"):
            self.assertEqual(
                len(result.get('Jack').get(status)),
                expected_count
            )

    def test_multilevel_level_pivot_with_serializer(self):
        qs = Todo.objects.all()
        result = pivot_table(qs, 'owner,status', serialize_todo)
        first_item = result.get("Jack").get("N")[0]
        assert isinstance(first_item, dict)


class PeriodicBreakdownTestCase(TestCase):

    def setUp(self):
        create_world()

    def test_periodic_breakdown(self):
        qs = Todo.objects.all()
        result = periodic_breakdown(qs, 'due')

        self.assertEqual(
            result[0].get('total'),
            3
        )
        self.assertEqual(
            result[1].get('total'),
            14
        )

    def test_periodic_breakdown_with_custom_aggregations(self):
        qs = Todo.objects.all()
        result = periodic_breakdown(
            qs,
            'due',
            aggregate_field='size',
            aggregation = 'Sum'
        )
        self.assertEqual(
            result[0].get('total'),
            6
        )
        self.assertEqual(
            result[1].get('total'),
            27
        )

# qs.annotate(date_only=Cast(search_field, DateField())).values('date_only')
